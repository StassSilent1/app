
@include ('layouts.header')
 <section class="pt-5">
     <div class="container">

         <div class="row">
             <div class="col-6">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <?php $k=0 ?>
                        @for($ii=0; $ii<sizeof($dep_table); $ii++)

                                    <th>{{$dep_table[$ii]->name}}</th>
                                    <?php $k++ ?>

                    @endfor

                    </tr>
                    </thead>
                    <tbody>
                    @for($i=0; $i<sizeof($emploe_table); $i++)
                        <tr>
                            <td>{{$emploe_table[$i]->name}}</td>
                            @for($j=0; $j<sizeof($dep_table); $j++)

                                @if(in_array($dep_table[$j]->id, explode(',', $emploe_table[$i]->department_id)))
                                    <td>+</td>
                                    @else
                                    <td></td>

                                @endif
                            @endfor
                            @endfor
                        </tr>

                    </tbody>
                </table>

             </div>
         </div>
     </div>
         </div>

 </section>
@include ('layouts.footer')
