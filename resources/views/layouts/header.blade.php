<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Главная</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

        <div class="d-flex flex-column flex-md-row justify-content-between">
            <div class="nav">
                    <a class="nav-link" href="/">Сетка</a>
                    <a class="nav-link" href="/departments">Отделы</a>
                    <a class="nav-link" href="/emploes">Сотрудники</a>
            </div>
        </div>
    </nav>
</header>



