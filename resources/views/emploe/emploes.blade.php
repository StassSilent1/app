@include('.layouts.header')
<section class="pt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">
                            ФИО
                        </th>
                        <th class="text-center">
                            Пол
                        </th>
                        <th class="text-center">
                            Отделы
                        </th>
                        <th class="text-center">
                           Заработная плата
                        </th>
                        <th class="text-center">
                            Действие
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($emploe_table as $e)
                        <tr id="emp{{$e->id}}">
                            <td class="text-center"> <span>{{$e->surname.' '.$e->name.' '.$e->patronymic}}</span></td>
                            <td class="text-center"> <span>{{$e->gender}}</span></td>
                            <td class="text-center"><span>{{$e->department}}</span></td>
                            <td class="text-center"><span>{{$e->salary}}</span></td>
                            <td  class="text-center">
                                <div class="edit-button"><a href="emploe/edit/{{$e->id}}" class="doings-button-link">редактировать</a></div>
                                <div class="doings-button-area"><button type="button" class=" del-emp doings-button" id="{{$e->id}}">удалить</button></div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="edit-button"><a href="emploe/add" class="doings-button-link">Добавить нового работника</a></div>
                <div>
                    {{--@foreach ($m as $n)--}}
                    {{--<span>{{$m}}</span>--}}
                    {{--@endforeach--}}
                </div>
            </div>
        </div>
    </div>
</section>

@include ('layouts/footer')