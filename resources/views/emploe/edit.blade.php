@include('.layouts/header')

<section class="pt-5">
    <div class="container">
        <div class="row pt-2">
            <div class="col-6 offset-md-2">
                <form id="emp-form">
                        <div class="pt-5">
                            <label class="label">Имя</label>
                                <input name="name"  @if ($currentPath =='emploe/edit/{id}') value="{{$user->name}}" @endif id="add_name_input" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">

                        </div>
                        <div class="pt-5">
                            <label class="label">Фамилия</label>
                            <input name="surname"  @if ($currentPath =='emploe/edit/{id}') value="{{$user->surname}}" @endif id="add_surname_input" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                         </div>
                        <div class="pt-5">
                            <label class="label">Отчество</label>
                            <input name="patronymic"  @if ($currentPath =='emploe/edit/{id}') value="{{$user->patronymic}}" @endif id="add_patronymic_input" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                        </div>
                        <div class="pt-5">
                            <label class="label">Пол</label>
                            <div>
                                <label class="label ">Женский</label><input  @if (($currentPath =='emploe/edit/{id}')&&($user->gender=="женский")) checked="checked" @endif  name="gender" id="add_gender_input" type="radio" value="женский" class="ml-2 form-check-input">
                                <label class="label pl-5">Мужской</label><input @if (($currentPath =='emploe/edit/{id}')&&($user->gender=="мужской")) checked="checked" @endif name="gender" id="add_gender_input1" type="radio" value="мужской" class="ml-2 form-check-input">
                            </div>
                        </div>

                    <div class="pt-5">
                        <label class="label">Заработная плата</label>
                        <input name="salary"  @if (($currentPath =='emploe/edit/{id}')&&($user->salary!=null)) value="{{$sal->salary}}" @endif id="add_salary_input" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                    </div>

                        <div class="pt-5">
                            <label class="label">Отдел</label>
                        <div>
                            @foreach($department as $d)
                                <span>{{$d->name}}</span><input @if ($currentPath =='emploe/edit/{id}') @foreach($dep as $d1) @if($d->id==$d1->department_id) checked="checked" @endif @endforeach  @endif name="dep[]" id="dep" type="checkbox" value="{{$d->id}}" class="mr-3 ml-2">
                            @endforeach

                        </div>
                        </div>
                                 @if($currentPath =='emploe/edit/{id}')
                                    <input type="hidden" value="{{$user->id}}" id="user_id">
                                    <div class="pt-5 "><button type="button" class="btn-primary btn-lg" id="edit_emp">Редактировать</button><div id="send_message-err" class="add-dep-er"></div></div>
                                     @else
                                    <div class="pt-5"><button type="button" class="btn-primary btn-lg edit-b" id="add_emp">Добавить</button><div id="send_message-err" class="add-dep-er"></div></div>
                                          @endif
                </form>
            </div>
        </div>
    </div>
</section>
@include ('layouts/footer')