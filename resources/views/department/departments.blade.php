@include('.layouts.header')
<section class="pt-5">
    <div class="container-fluid">
         <div class="row">
              <div class="col-12">
                   <table class="table table-hover">
                        <thead>
                        <tr>
                             <th class="text-center">
                                  Отдел
                             </th>
                             <th class="text-center">
                                  Кодичество сотрудников
                             </th>
                             <th class="text-center">
                                  Максимальная заработная плата среди сотрудников

                             <th class="text-center">
                                  Действие
                             </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($department_table as $t)
                             <tr id="dep{{$t->id}}">
                                  <td class="text-center"> <span>{{$t->department}}</span></td>
                                  <td class="text-center"> <span>{{$t->count}}</span></td>
                                  <td class="text-center"><span>{{$t->max}}</span></td>
                                  <td  class="text-center">
                                       <div class="edit-button"><a href="department/edit/{{$t->id}}" class="doings-button-link">редактировать</a></div>
                                       <button type="button" class="doings-button del-dep" id="{{$t->id}}">удалить</button>

                                  </td>
                                 <td><span id="errors{{$t->id}}"></span></td>
                             </tr>
                        @endforeach
                        </tbody>
                   </table>
                   <div class="edit-button"><a href="department/add" class="doings-button-link">Добавить новый отдел</a></div>
                   <div>
                   </div>
              </div>
         </div>
    </div>
</section>

@include ('layouts/footer')