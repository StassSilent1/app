@include('.layouts/header')

    <section class="pt-5">
        <div class="container">
            <div class="row pt-5">
                <div class="col-6 offset-md-2 ">
                    <form>
                        <label class="label">Название отдела</label>
                        @if ($currentPath ==('department/add'))

                                <div class="input-group input-group-lg">
                                    <input name="department" id="add_dep_input" type="text" class="form-control input-add" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            <div class="pt-5"><button type="button" class="btn-primary btn-lg" id="add_dep">Добавить</button><div id="send_message-err" class="add-dep-er"></div>
                        @else
                                <div class="input-group input-group-lg">
                                    <input name="department" value="{{$name}}" id="edit_dep_input" type="text" class="form-control input-edit" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            <div class="pt-5"><button type="button" class="btn-primary btn-lg" id="edit_dep" >Редактировать</button><div id="send_message-err" class="add-dep-er"></div>
                            <input type="hidden" id="id_add_dep" value="{{$id}}">
                            </div>

                        @endif

                    </form>
                </div>


            </div>
        </div>
    </section>
@include ('layouts/footer')