<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Emploe;
use App\Department;
use DB;
class Compare extends Model
{
    protected $table = 'compare';

    protected $fillable = [
        'emploe_id', 'department_id',
    ];
    public function emploe()
{
    return $this->belongsTo(Emploe::class);
}

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }



}
