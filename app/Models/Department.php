<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Emploe;
use App\Compare;
use DB;

class Department extends Model
{
    protected $table = 'department';

    protected $fillable = [
        'name',
    ];
    public function compare()
    {
        return $this->hasMany(Compare::class,'department_id','id')->select(['department_id'=>'id']);
    }
    public function get_department()
    {
        $department_table = $this->
        select('department.name as department', 'department.id as id', DB::raw('count(emploe.id) as count'), DB::raw('max(salary.salary) as max'))
            ->leftJoin('compare', 'department.id', '=', 'compare.department_id')
            ->leftJoin('emploe', 'emploe.id', '=', 'compare.emploe_id')
            ->leftJoin('salary', 'emploe.id', '=', 'salary.emploe_id')
            ->groupby('department.id')
            ->get();

        return $department_table;
    }
       public function delete_department($id)
        {
            if($this->find($id)->compare()->count()==0)
            {
            $this->find($id)->delete();
            $error=0;
            }
            else $error=1;
            return $error;
        }
        public function add_department($value)
        {
            if ($value == null)
                $error = 1;
            else {
                $error = 0;
                $this->name = $value;
                $this->save();

            }
            return $error;
        }
    public function edit_department($value, $id)
    {
        if ($value == null)
            $error = 1;
        else {
            $error = 0;
            $this->where('id', $id)->update(['name' => $value]);

        }
        return $error;
    }





}
