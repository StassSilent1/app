<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Compare;
class Emploe extends Model
{
    protected $table = 'emploe';

    protected $fillable = [
        'name', 'surname', 'patronymic', 'gender',
    ];

    public function salary()
    {
        return $this->hasOne(Salary::class,'emploe_id','id')->select(['salary'=>'salary']);
    }
    public function compare()
    {
        return $this->hasMany(Compare::class,'emploe_id','id')->select(['department_id'=>'department_id']);
    }

    function get_emploe(){
        $emploe_table=$this->
        select('emploe.name as name', 'salary.salary as salary', 'emploe.id as id', 'emploe.surname as surname', 'emploe.patronymic as patronymic', 'emploe.gender as gender',DB::raw('group_concat(department.name) as department'),DB::raw('group_concat(department.id) as department_id'))
            ->leftJoin('compare', 'emploe.id', '=', 'compare.emploe_id')
            ->leftJoin('department', 'department.id', '=', 'compare.department_id')
            ->leftJoin('salary', 'emploe.id', '=', 'salary.emploe_id')
            ->groupby('emploe.id')
            ->get();
        return $emploe_table;

    }
    function delete_emploe($id)
    {
            return $this->find($id)->delete();

    }

    public function add_emploe($name,$surname,$patronymic,$gender, $salary, $dep)
    {
        if ($name == null)
            $error = 1;
        else  if ($surname == null)
            $error = 2;
        else if (empty($dep))
            $error = 3;
        else
            {
            $error = 4;
            DB::beginTransaction();
            try {
                $this->name = $name;
                $this->surname = $surname;
                $this->patronymic = $patronymic;
                $this->gender = $gender;
                $this->save();

                $id=DB::getPdo()->LastInsertID();

                foreach ($dep as $d)
                {
                    $c = new Compare();
                    $c->department_id = $d;
                    $c->emploe_id =$id;
                    $c->save();
                }
                if ($salary!=null) {
                    $s = new Salary();
                    $s->emploe_id = $id;
                    $s->salary = $salary;
                    $s->save();
                }
            }
            catch (QueryExeption $e){
                DB::rollBack();
            }
            DB::commit();



        }
        return $error;
    }

    public function edit_emploe($id, $name,$surname,$patronymic,$gender, $salary, $dep)
    {
        if ($name == null)
            $error = 1;
        else  if ($surname == null)
            $error = 2;
        else if (empty($dep))
            $error = 3;
        else
        {
            $error = 4;
            DB::beginTransaction();
            try {
                if ($salary == null) {
                    $this->find($id)->salary()->delete();
                }
                else if (($salary != null)&&($this->find($id)->salary))
                {
                    $this->find($id)->salary()->update(['salary'=>$salary]);
                }
                else{
                    $s = new Salary();
                    $s->emploe_id=$id;
                    $s->salary=$salary;
                    $s->save();
                }
                $this->where('id', $id)->update(['name' => $name, 'surname'=>$surname, 'patronymic' => $patronymic, 'gender' => $gender]);


                    $this->find($id)->compare()->delete();
                foreach ($dep as $d)
                {
                    $c = new Compare();
                    $c->department_id = $d;
                    $c->emploe_id =$id;
                    $c->save();
                }
            }
            catch (QueryExeption $e){
                DB::rollBack();
            }
            DB::commit();



        }
        return $error;
    }


}

