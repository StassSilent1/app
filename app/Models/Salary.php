<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = 'salary';

    protected $fillable = [
        'salary'
    ];

    public function employe()
    {
        return $this->hasOne(Emploe::class);
    }

}
