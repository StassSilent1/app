<?php

namespace App\Http\Controllers;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class DepartmentController extends Controller
{

    public function __construct(Department $department)
    {
        $this->department = $department;
    }


    public function getDepartment()
    {

        $department_table = $this->department->get_department();

        return view('department/departments', compact('department_table'));
    }

    public function deleteDepartment(Request $request)
    {

        $er = $this->department->delete_department($request->id);
        return $er;
    }

   public function getAdd()
    {
        $currentPath = Route::getFacadeRoot()->current()->uri();
        return view('department/edit', compact('currentPath'));
    }

    public function postAdd(Request $request)
    {
        $er= $this->department->add_department($request->value);

        return $er;

    }

    public function getEdit($id)

    {
        $currentPath = Route::getFacadeRoot()->current()->uri();
        $name = $this->department->find($id)->name;

        return view('department/edit', compact('currentPath', 'name', 'id'));
    }

    public function postEdit(Request $request)
    {
        {
           $er=$this->department->edit_department($request->value, $request->id);
            return $er;
        }


    }
}


