<?php

namespace App\Http\Controllers;

use App\Compare;
use App\Department;
use App\Emploe;
use Illuminate\Http\Request;

class indexController extends Controller
{
    public function getIndex(){

        $dep = new Department();
        $dep_table = $dep->get();
        $emp= new Emploe();
        $emploe_table=$emp->get_emploe();

       return view('index', compact('dep_table', 'emploe_table') );


    }

}
