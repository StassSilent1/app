<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emploe;
use App\Department;
use Illuminate\Support\Facades\Route;
class EmploeController extends Controller
{

    public function __construct(Emploe $emploe)
    {
        $this->emploe = $emploe;
    }

    public function getEmploe()
    {

        $emploe_table=$this->emploe->get_emploe();

        return view('emploe/emploes', compact('emploe_table'));
    }
    public function deleteEmploe(Request $request)
    {

        $this->emploe->delete_emploe($request->id);


    }
    public function getAdd(Department $department)
    {
        $currentPath = Route::getFacadeRoot()->current()->uri();
        $department=$department->all();


        return view('emploe/edit', compact('currentPath','department'));
    }
    public function postAdd(Request $request)
    {
        $err= $this->emploe->add_emploe($request->name, $request->surname, $request->patronymic, $request->gender, $request->salary, $request->dep);
        return $err;
    }
    public function getEdit($id, Department $dep_id)

    {
        $currentPath = Route::getFacadeRoot()->current()->uri();

        $user = $this->emploe->find($id);
        $dep=$this->emploe->find($id)->compare;
        $sal=$this->emploe->find($id)->salary;
        $department=$dep_id->all();

        return view('emploe/edit', compact('currentPath', 'user', 'dep','department','sal'));
    }
    public function postEdit(Request $request)

    {

        $err= $this->emploe->edit_emploe($request->id, $request->name, $request->surname, $request->patronymic, $request->gender, $request->salary, $request->dep);
        return $err;
    }


}

