$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.del-dep').on('click', function () {
        var id=$(this).attr('id');//id
        console.log(id);
        $.ajax({
            url: 'departments1',
            type: "POST",
            data: {id:$(this).attr('id')},
            dataType: 'html',
            success: function (msg) {
                if (msg!=0){
                    $('#errors'+id).text("Здесь есть работники!")
                }
                else
                $('#dep'+id).css('display','none');
            }
        });
    });

    $('#add_dep').on('click', function (e) {
        // e.preventDefault();
        var value=$('#add_dep_input').val();//value
        console.log(value);
        $.ajax({
            url: '/department/add1',
            type: "POST",
            data: {value:value},
            dataType: 'html',
            success: function (msg) {
                console.log(msg);
                if (msg==0){
                    $('#send_message-err').text("Данные отправлены");
                    $('#add_dep_input').val('');
                }
                else
                    $('#send_message-err').text("Заполните поле")
            }
            });
    });

    $('#edit_dep').on('click', function (e) {
        // e.preventDefault();
        var value=$('#edit_dep_input').val();//value
        var id=$('#id_add_dep').val();//id
        console.log(value);
        $.ajax({
            url: '/department/edit1',
            type: "POST",
            data: {value:value, id:id},
            dataType: 'html',
            success: function (msg) {
                console.log(msg);
                if (msg==0){
                    $('#send_message-err').text("Изменения внесены");
                    $('#add_dep_input').val('');
                }
                else
                    $('#send_message-err').text("Заполните поле")
            }
        });
    })

    $('.del-emp').on('click', function () {
        var id=$(this).attr('id');//id
        console.log(id);
        $.ajax({
            url: 'emploes1',
            type: "POST",
            data: {id:$(this).attr('id')},
            dataType: 'html',
            success: function (msg) {
                // if (msg!=0){
                //     $('#errors'+id).text("Здесь есть работники!")
                // }
                // else
                    $('#emp'+id).css('display','none');
            }
        });
    });

    $('#add_emp').on('click', function () {
        var name=$('#add_name_input').val();
        var surname=$('#add_surname_input').val();
        var patronymic=$('#add_patronymic_input').val();
        var gender=$('input[type="radio"]:checked').val();
        var salary=$('#add_salary_input').val();

        var dep=new Array();

        $('input[type="checkbox"]:checked').each(function()
            {
                dep.push($(this).val());
            }
        );
        console.log(dep);
        console.log(salary);
        // console.log(name+surname+patronymic+gender+dep );
        $.ajax({
            url: '/emploe/add1',
            type: "POST",
            data: {name:name, surname:surname, patronymic:patronymic, gender:gender, dep:dep, salary:salary},
            dataType: 'html',
            success: function (msg) {
                console.log(msg);
                if (msg==1){
                    $('#send_message-err').text("Заполните имя");
                }
                else if (msg==2){
                    $('#send_message-err').text("Заполните фамилию");
                }
                else if (msg==3){
                    $('#send_message-err').text("Выберете отдел");
                }
                else {
                    $('#send_message-err').text("Данные отправлены");
                    $('#emp-form')[0].reset();
                }



            }
        });
    });


    $('#edit_emp').on('click', function () {
        var id=$('#user_id').val();
        var name=$('#add_name_input').val();
        var surname=$('#add_surname_input').val();
        var patronymic=$('#add_patronymic_input').val();
        var gender=$('input[type="radio"]:checked').val();
        var salary=$('#add_salary_input').val();
        var dep=new Array();

        $('input[type="checkbox"]:checked').each(function()
            {
                dep.push($(this).val());
            }
        );
        console.log(dep);
        // console.log(name+surname+patronymic+gender+dep );
        $.ajax({
            url: '/emploe/edit1',
            type: "POST",
            data: {name:name, surname:surname, patronymic:patronymic, gender:gender, dep:dep, id:id, salary:salary},
            dataType: 'html',
            success: function (msg) {
                console.log(msg);
                if (msg==1){
                    $('#send_message-err').text("Заполните имя");
                }
                else if (msg==2){
                    $('#send_message-err').text("Заполните фамилию");
                }
                else if (msg==3){
                    $('#send_message-err').text("Выберете отдел");
                }
                else
                    $('#send_message-err').text("Данные отправлены");

            }
        });
    });


});

