<?php

use Illuminate\Database\Seeder;

class SalaryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('salary')->insert(
                array(
                    ['id'=>'1',
                        'emploe_id'=>'1',
                        'salary'=>'50000',


                    ],
                    ['id'=>'2',
                        'emploe_id'=>'2',
                        'salary'=>'30000',

                    ],
                    ['id'=>'3',
                        'emploe_id'=>'3',
                        'salary'=>'40000',

                    ],
                    ['id'=>'4',
                        'emploe_id'=>'4',
                        'salary'=>'30000',

                    ],
                    ['id'=>'5',
                        'emploe_id'=>'5',
                        'salary'=>'30000',

                    ],
                )
            );
            //

        }
    }
}
