<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('department')->insert(
            array(
                ['id'=>'1',
                    'name'=>'продаж',

                ],
                ['id'=>'2',
                    'name'=>'кадровый',


                ],
                ['id'=>'3',
                    'name'=>'бухгалтерия',


                ],
                ['id'=>'4',
                    'name'=>'разработки',


                ],
                ['id'=>'5',
                    'name'=>'закупок',


                ],
                ['id'=>'6',
                    'name'=>'планирования',


                ],
            )
        );
            //

    }
}
