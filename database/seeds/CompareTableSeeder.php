<?php

use Illuminate\Database\Seeder;

class CompareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('compare')->insert(
            array(
                ['id'=>'1',
                    'emploe_id'=>'1',
                    'department_id'=>'1',


                ],
                ['id'=>'2',
                    'emploe_id'=>'2',
                    'department_id'=>'1',

                ],
                ['id'=>'3',
                    'emploe_id'=>'2',
                    'department_id'=>'2',

                ],
                ['id'=>'4',
                    'emploe_id'=>'3',
                    'department_id'=>'3',

                ],
                ['id'=>'5',
                    'emploe_id'=>'4',
                    'department_id'=>'4',

                ],
                ['id'=>'6',
                    'emploe_id'=>'5',
                    'department_id'=>'5',

                ],
            )
        );
            //

    }
}
