<?php

use Illuminate\Database\Seeder;

class EmploeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { DB::table('emploe')->insert(
        array(
            ['id'=>'1',
                'name'=>'Наталья',
                'surname'=>'Петрова',
                'patronymic'=>'Олеговна',
                'gender'=>'женский',

            ],
            ['id'=>'2',
                'name'=>'Алексей',
                'surname'=>'Петров',
                'patronymic'=>'Олегович',
                'gender'=>'мужской',

            ],
            ['id'=>'3',
                'name'=>'Игорь',
                'surname'=>'Игорев',
                'patronymic'=>'Игоревич',
                'gender'=>'мужской',

            ],
            ['id'=>'4',
                'name'=>'Елена',
                'surname'=>'Данилова',
                'patronymic'=>'Сергеевна',
                'gender'=>'женский',

            ],
            ['id'=>'5',
                'name'=>'Михаил',
                'surname'=>'Михаилов',
                'patronymic'=>'Михаилович',
                'gender'=>'мужской',

            ],

        )
    );
        //
    }
}
