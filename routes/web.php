<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "indexController@getindex");

Route::get('departments', "DepartmentController@getDepartment");
Route::post('departments1', "DepartmentController@deleteDepartment");
Route::get('department/add', "DepartmentController@getAdd");
Route::post('department/add1', "DepartmentController@postAdd");
Route::get('department/edit/{id}', "DepartmentController@getEdit");
Route::post('department/edit1', "DepartmentController@postEdit");

Route::get('emploes', "EmploeController@getEmploe");
Route::post('emploes1', "EmploeController@deleteEmploe");
Route::get('emploe/add', "EmploeController@getAdd");
Route::post('emploe/add1', "EmploeController@postAdd");
Route::get('emploe/edit/{id}', "EmploeController@getEdit");
Route::post('emploe/edit1', "EmploeController@postEdit");


